local player = {}

function player.init()
    return {
        x = love.graphics.getWidth() / 2,
        y = love.graphics.getHeight() / 2,
        actions = {
            idle = {
                sprite = love.graphics.newImage("assets/keyframes/gold_axolotl_idle_in_water.png"),
                quad_width = 97,
                quad_height = 56,
                animation = {
                    direction = "right",
                    frame = 1,
                    max_frames = 20,
                    speed = 20,
                    timer = 0.1
                }
            },
            swim = {
                sprite = love.graphics.newImage("assets/keyframes/gold_axolotl_swim.png"),
                quad_width = 97,
                quad_height = 56,
                animation = {
                    direction = "right",
                    frame = 1,
                    max_frames = 8,
                    speed = 2,
                    timer = 0.1
                }
            },
            jump = {
                sprite = love.graphics.newImage("assets/keyframes/gold_axolotl_jump.png"),
                quad_width = 97,
                quad_height = 56,
                animation = {
                    direction = "right",
                    frame = 1,
                    max_frames = 11,
                    speed = 2,
                    timer = 0.1
                }
            }
        },
        quads = {
            idle = {},
            swim = {},
            jump = {}
        },
        createQuads = function(self)
            for action in pairs(self.actions) do
                for i = 1, (self.actions[action].animation.max_frames) do
                    self.quads[action][i] =
                        love.graphics.newQuad(
                        self.actions[action].quad_width * (i - 1),
                        0,
                        self.actions[action].quad_width,
                        self.actions[action].quad_height,
                        self.actions[action].sprite:getWidth(),
                        self.actions[action].sprite:getHeight()
                    )
                end
            end
        end
    }
end

return player
