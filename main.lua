if pcall(require, "lldebugger") then
    require("lldebugger").start()
end

io.stdout:setvbuf("no")

love.graphics.setDefaultFilter("nearest")

local player = require "player"

function love.load()
    player = player.init()
    quads = player:createQuads()

    action = "idle"
end

function love.update(dt)
    if love.keyboard.isDown("right") then
        action = "swim"
        player.actions[action].animation.direction = "right"
        player.actions.idle.animation.direction = "right"
        player.actions.jump.animation.direction = "right"
        player.x = player.x + player.actions[action].animation.speed
    elseif love.keyboard.isDown("left") then
        action = "swim"
        player.actions[action].animation.direction = "left"
        player.actions.idle.animation.direction = "left"
        player.actions.jump.animation.direction = "left"
        player.x = player.x - player.actions[action].animation.speed
    elseif love.keyboard.isDown("down") then
        action = "swim"
        player.y = player.y + player.actions[action].animation.speed
    elseif love.keyboard.isDown("up") then
        action = "swim"
        player.y = player.y - player.actions[action].animation.speed
    elseif love.keyboard.isDown("space") then
        action = "jump"
    else
        action = "idle"
    end

    player.actions[action].animation.timer = player.actions[action].animation.timer + dt

    if player.actions[action].animation.timer > 0.2 then
        player.actions[action].animation.timer = 0.1
        player.actions[action].animation.frame = player.actions[action].animation.frame + 1
        if player.actions[action].animation.frame > player.actions[action].animation.max_frames then
            player.actions[action].animation.frame = 1
        end
    end
end

function love.draw()
    if player.actions[action].animation.direction == "right" then
        love.graphics.draw(
            player.actions[action].sprite,
            player.quads[action][player.actions[action].animation.frame],
            player.x,
            player.y,
            0,
            -1,
            1,
            player.actions[action].quad_width,
            0
        )
    elseif player.actions[action].animation.direction == "left" then
        love.graphics.draw(
            player.actions[action].sprite,
            player.quads[action][player.actions[action].animation.frame],
            player.x,
            player.y
        )
    end
end
